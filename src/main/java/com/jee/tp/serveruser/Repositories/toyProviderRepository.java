package com.jee.tp.serveruser.Repositories;

import com.jee.tp.serveruser.Models.toyProvider;
import org.springframework.data.jpa.repository.JpaRepository;

public interface toyProviderRepository extends JpaRepository<toyProvider, Long> {
}
